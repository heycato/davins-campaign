# Character: Beq
```
Race: Goblin
Class: Cleric (Tempest Domain)
Background: Sailor
Age: approx. 38
```
![selfy](http://conceptartworld.com/wp-content/uploads/2013/11/Bryan_Wynia_Art_Goblin-Raider.jpg)
Image used with permission from
[Bryan Wynia](https://www.artstation.com/bryanwynia).
## TLDR
Beq was found on the shore of Hull Bay by halflings, Finnin and Wiri Tolgard, and taken to Lockinge where they raised him.  Finnin taught him to sail, and Wiri gave him his education.  When they died, He went to live with Nedalyn, the town's cleric, who taught him how to persuade the weather.  After Nedalyn passed, he went to the Port of Stocktrie, where he found work on a cargo ship for 17 years.  The ship was wrecked by a storm and only Beq survived.  Now he's back in the port he started at, not seeking to join another crew, but pondering what he's going to do next.

## Backstory
I was told they found me wandering the shoreline of Hull Bay near the halfling village of Lockinge, just after a severe storm had passed through.  Finnin Tolgard and his wife Wiri were walking along the beach when they spotted me, a small child stumbling around, disoriented and alone.  They say when they came near to me, they could see I was ragged and soaked, staring out at the sea, presumably where I'd come from.  Knowing right away from my appearance that I wasn't from the area, Finnin searched the beach for wreckage while Wiri scooped me up and took me back to their house in the village.  We don't really know my age of course, but when they found me they presumed I was only about one or two years old.  They called me Bequest, claiming that I was a sort of gift from the storm and sea.

They brought me up as best they could, being how they had no children of their own, and they themselves were in their later years.  I wasn't well received by the community however, and they all but shunned my parents and me.  I was often taunted and ridiculed by the other children when we were in the market square, so I took to wearing a hood and scarf to hide my appearance.

Finnin was a fisherman by trade, so from the time I was deemed old enough, I spent most of my days out on his skiff.  He taught me everything I know about navigating the seas.  He died of old age when I was a teen. Wiri died two years later. I learned reading and writing from her, as well as how to keep a garden.  They introduced me to the healer in Lockinge, a cleric named Nedalyn. After my parents died, Nedalyn had compassion on my circumstance and took me in.  She followed a path more aligned with light, but gave me space to figure out my own path.  Nedalyn had the ability to manipulate fire and light and taught me how to similarly persuade the weather.  I wanted to control the storm so I could make it take me back to where I came from, but she showed me that is not possible.  One can only tune themselves to it's force and energy, and influence it.  "Just like with you and me...", she would say, "...no one has control over us, but those with charm and cunning, can persuade us to action or inaction.".

When Nedalyn passed, I took my father's skiff and sailed south east along the shore until I came to the port city of Stocktrie, hoping to find answers about my origin.  There I found work as a crewman for the freight hauler The Mavourneen.  Sailors come in all kinds, so I quickly found acceptance among my captain and shipmates, and for the first time in my life, felt a sense of belonging.

I worked on The Mavourneen for 17 years, the last 10 years as the ship's Sailing Master (navigator).  We traveled all lengths of the Everlis Sea even down to the Lampar Depths in the far south.  Last year, during a run from Dolcarres to Cona Harbor, we were overtaken by a hurricane.  I pleaded with the storm to pass us, but it would not yield.  Our main sail was shredded in an instant, and the storm pushed us off course into reef where we lost the rudder and gained a sizeable breach in the hull.  Several of the crew were swept off the deck by the wind and waves, and by the time the storm had passed, there were only a few of us left alive.  We survived another day floating along the current until the ship took on more water than we could bail and sank like a stone to the bottom of the sea.  The remaining crew members didn't escape, and I was alone floating on the surface with a few crates of our cargo.  Another few days on the current, and another storm swept in during the middle of the night, and carried me until I reached the shore.  A fisherman spotted me and took me in for a few days, and when I had strength enough to travel, I made out for Cona Harbor.  From there I caught a ride with another freighter, The Aveley, which took me back to Stocktrie.  I've been here now for 10 months, scraping barnacles off of ships in the dockyard.  I'm not ready to get back on the sea with another crew just yet, possibly never.  The loss of my shipmates has hit me harder than I ever would have thought.  Maybe storms have brought me back to shore for a reason, maybe it's time for me to find out who and what I am.

## Facts and Lore

#### Nedalyn
- Beq didn't know when he was being taught by her, but later found out that Nedalyn was of some renown.  Apparently, before she settled in Lockinge, she travelled with a well-known group of adventurers, the "Champions of Vel'Nor", that helped defend the region from the "Dark Ones" from the Temple of Tisilon, located beyond the North Boundary.

#### The Mavourneen and The Aveley
- The crew of The Mavourneen was like Beq's family, the only place outside of the Tolgard home that he's ever found acceptance.
		- A few key members of the crew:
				- Captain Bil Brenik (male dwarf)
				- Firstmate Mr. Basson (male human)
				- Boatswain (Bos'n) Marigold Whitbottom (female halfling)
- The Mavourneen and The Aveley were often hired by the same charterer, so they worked together often on the same shipments.
- Beq worked with the captain of The Aveley, a female half-elf named Charlie Vossler, for short season when The Mavourneen was docked for repairs.  She has offered him a job a few times since The Mavourneen went down, but he declined.
